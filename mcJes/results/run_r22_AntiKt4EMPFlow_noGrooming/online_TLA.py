from BinnedPhaseSpace.BinnedPhaseSpaceConfig import AxisSpec , BinnedPhaseSpace, BinnedTH1, BinnedTH2, BinnedTGraph2DErrors, h1, h2
from DeriveJetScales.PyHelpers import NIOptions

# **************************
# retrieve C++ objects.
# we test if a ResponseBuilder class has already been defined. If not we default to the C++ implementation in ROOT.DJS
if 'ResponseBuilder' not in dir():
    ResponseBuilder = ROOT.DJS.ResponseBuilder
# we test if a CalibrationObjects class has already been defined. If not we default to the C++ implementation in ROOT.DJS
if 'CalibrationObjects' not in dir():
    CalibrationObjects = ROOT.DJS.CalibrationObjects

# the above allows to exchange the default C++ class by debugging or validation classes.
# ********************************
# By default take the jetType from the script arguments
defaultjetType = args.jetType
#defaultjetType = "AntiKt10LCTopoTrimmedPtFrac5SmallR20" # uncomment to force to a given type
# ********************************

def configure():

    # ######################################################
    # ######################################################
    # EtaJES / JMS common configuration 
    
    # Build a CalibrationObjects which will centralize every
    # collections of TH1/TH2/TGraphErrors/TF1 needed for JES/JMS derivation 
    objs = CalibrationObjects()
    # this is a C++ object (see Utilities.h) but we will augment it with pure python attributes

    objs.AtlasLabel = "Simulation Internal"
    objs.jetType = defaultjetType
    objs.jetTypePublic = ["a4tcemsubjesISFS"]
    objs.radius  = 0.4

    inputReader = ROOT.DJS.InputReaderVector()
    objs.inputReader = inputReader

    # set input branch name :
    inputReader.m_evntWeight_bname = "weightreweighted"
    inputReader.m_e_true_bname =  "jet_true_e"
    inputReader.m_e_reco_bname = "jet_E"
    inputReader.m_eta_reco_bname = "jet_eta"
    inputReader.m_eta_det_bname = "jet_DetEta"# same as DetPileupEta
    inputReader.m_eta_true_bname ="jet_true_eta"
    inputReader.m_m_true_bname = "jet_true_m"
    inputReader.m_m_reco_bname = "jet_m"
    inputReader.m_mu_bname = "actualInteractionsPerCrossing"

    respBuilder = ResponseBuilder()
    objs.respBuilder = respBuilder

    respBuilder.m_inputReader = inputReader

    # ######################################################
    # ######################################################
    # EtaJES configuration 

    # eta bins
    etaBins = AxisSpec("EtaBins", "#eta", seq(-4.5 , 4.5, 0.1 ) )
    etaBins.m_isGeV = False
    objs.etaBins = etaBins
    
    
    # Histogram bins
    eBins = [20, 25, 30, 35, 40, 50, 60, 80, 100, 120, 150, 200, 240, 300, 400, 500, 600,
             800, 1000, 1200, 1500, 2000, 2500, 3000, 3500, 4000, 5500 ]

    pBins = [ 20, 25, 30, 35, 40, 50, 60, 80, 100, 120, 150, 200, 240, 300, 400, 500, 600,
              800, 1000, 1200, 1500, 2000, 2500, 3000, 4000]

    # response bins 
    #rBins = seq(0,2.4, 0.01)
    rBins = (240, 0, 2.4) # Histo defined with this  produces different fit although the binning is actually identical

    objs.defineHistos( # this function is defined in PyHelpers
        # each line defines a histogram which will be cloned for each eta bin
        #  (h1 and h2 are utilities which only convert python lists into arguments usable by TH1 ctors)
        R_vs_Etrue =     ("R_vs_Etrue","")+ h2(eBins, *rBins ) ,
        R_vs_Ereco =     ("R_vs_Ereco","")+ h2(eBins, *rBins ) ,
        R_vs_pTtrue =     ("R_vs_pTtrue","")+ h2(pBins, *rBins ) ,
        R_vs_mu =        ("R_vs_mu","")+h2(5, 40., 70., *rBins),
        Etrue_vs_Etrue = ("Etrue_vs_Etrue","")+ h1(eBins ) , 
        Ereco_vs_Ereco = ("Ereco_vs_Ereco","")+ h1(eBins ) , 
        pTtrue_vs_pTtrue = ("pTtrue_vs_pTtrue","")+ h1(pBins ) , 
        Ereco_vs_Etrue = ("Ereco_vs_Etrue","")+ h1(eBins ) ,
        DEta_vs_Etrue =  ("DEta_vs_Etrue","")+ h2(eBins,1400,-0.2,0.2 ) , 
        R_vs_Enuminv  =  ("R_vs_Enuminv","")+ h2(eBins, *rBins ) ,
        Enuminv_vs_Etrue = ("Enuminv_vs_Etrue","")+ h1(eBins ) ,
        closure_R_vs_Etrue =     ("closure_R_vs_Etrue","")+ h2(eBins, *rBins ) ,
        closure_R_vs_pTtrue =     ("closure_R_vs_pTtrue","")+ h2(pBins, *rBins ) ,
        closure_DEta_vs_Etrue =     ("closure_DEta_vs_Etrue","")+ h2(eBins, 1400,-0.2,0.2  ) ,
        )


    # ********************************************
    # configure the response builder

    respBuilder.m_etaBins = etaBins
    respBuilder.m_objects = objs
    # EtaJES config
    respBuilder.m_jesCorr.m_minPtExtrapolated = 15 # default = 10, determines the corresponding  Ecutoff below which the curve is extrapolated istead of using the actual fit in tha range.
    respBuilder.m_jesCorr.m_minPtETACorr = 8
    respBuilder.m_jesCorr.m_maxEETACorr = 2500

    # ********************************************
    # configure the fitters
    objs.jesRespEvaluation = "gaus" # "mode" # default is gaus
    
    eRespFitter = ROOT.DJS.ResponseFitter()
    eRespFitter.m_minNEffP1 = -3.1
    eRespFitter.m_minNEffP2 = 1.05
    eRespFitter.m_minNEffP3 = 400
    eRespFitter.m_NSigmaForFit = 1.3 # this is for energy !!
    eRespFitter.m_minEtForMeasurement =25#13(good)#11mine#15(tostrong)#10(close)#7 #0#20(default)

    eRespFitter.m_useBetterChi2 = False
    eRespFitter.m_useGeoDetCorr = True
    eRespFitter.m_minGeoDetCorr = 0.8
    eRespFitter.m_maxGeoDetCorr = 1.0

    # not set for AntiKt10 :
    #eRespFitter.m_etaException
    #eRespFitter.m_JES_MaxOrderException 


    eRespFitter.m_JES_Function = "polBestChi2"
    #eRespFitter.m_JES_Function = "polBestChi2+groom"
    eRespFitter.m_JES_MaxOrderP1 = -3.1
    eRespFitter.m_JES_MaxOrderP2 =-0.05
    eRespFitter.m_JES_MaxOrderP3 =6.5#7.5 #6.5 (josef think )# 5.5(original)


    eRespFitter.m_etaCorr_MaxOrderP1 = -3.0
    eRespFitter.m_etaCorr_MaxOrderP2 = -0.25
    eRespFitter.m_etaCorr_MaxOrderP3 = 3.95



    # GCW and LC not well descriped in low E region by groom function
    # EnableEMinExtrapolation: false gives better results in these cases
    eRespFitter.m_enableEMinExtrapolation = True #False    
    eRespFitter.m_enableEMaxExtrapolation = True # (default value :True)
    #eRespFitter.m_enableEMaxExtrapolationException -> not set for AntiKt10
    #eRespFitter.m_enableEMinExtrapolationException -> not set for AntiKt10
    
    eRespFitter.m_maxAcceptableYDeviationFitVSMinGroom = 0.3

    # In case the number of data points in the R vs. E graph < MinNDataPointsForMinExtrapolation
    # => EnableEMinExtrapolation=true forced ("JES_Function: polBestChi2+groom" only)
    eRespFitter.m_minNDataPointsForMinExtrapolation = 3

    eRespFitter.m_minEtForExtrapolation =5.0
    eRespFitter.m_maxEtForFits = 4000 #orig 3500
    

    balanceFitter = ROOT.JES_BalanceFitter(eRespFitter.m_NSigmaForFit)
    balanceFitter.SetGaus()
    balanceFitter.SetFitOpt("REQS")
    eRespFitter.m_respFitter = balanceFitter

    eRespFitter.m_etaBins = etaBins

    eRespFitter.m_useMode = (objs.jesRespEvaluation == "mode")

    objs.eRespFitter = eRespFitter


    # ******************************
    # config a fitter for delta Eta
    dEtaFitter = ROOT.DJS.ResponseFitter(eRespFitter)
    # same config as eRespFitter except the following :
    dEtaFitter.m_NSigmaForFit = 1.3 #orig 1.4

    balanceFitter = ROOT.JES_BalanceFitter(dEtaFitter.m_NSigmaForFit)
    balanceFitter.SetGaus()
    balanceFitter.SetFitOpt("REQS")
    dEtaFitter.m_respFitter = balanceFitter
    dEtaFitter.m_etaBins = etaBins
    dEtaFitter.m_useMode = (objs.jesRespEvaluation == "mode")
    dEtaFitter.m_respHistTitle = "#Delta Eta, %.1f < E_{true} < %.1f, etabin=%d";

    objs.dEtaFitter = dEtaFitter



    
    # ######################################################
    # ######################################################
    # JMS configuration 

    # ******************************
    # optionnal JMS strategies. By default, all are set to False
    jmsNIopts = NIOptions(
        noNumInv=False,
        directNumInv=False,
        flattenCalib=False,
        adjustFlatCalib=False,
        forceSuperIncrResp=False
    ) 
    objs.jmsNIopts = jmsNIopts 
    objs.useDirectNumInv  = False
    objs.jmsPreCorr       = False
    objs.jmsRespEvaluation = "gaus" # default is "gaus", alternative is "mode"
    objs.binSystem = "ELogMoEBins"
    #objs.binSystem = "MLogMoEBins"
    #objs.binSystem = "PtMBins"
    # ******************************
    
    
    jmsEtaBins = [0,0.2,0.6, 1.0 , 1.4, 2., 3  ]
    if objs.binSystem=="PtMBins":
        BinningClass = ROOT.DJS.PtMBinning
        objs.pt_desc = 'p_{T} [GeV]'
        objs.m_desc = 'm [GeV]'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("pt", "p_{T} true", seq(200,1000,100)+seq(1200,2000,200)+[2500,3000,4000,5000], isGeV=True ),
                                   AxisSpec("m", "m true",seq(0,200,10)+[225,250,275,300], isGeV=True),
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
                               )
    elif objs.binSystem=="ELogMoEBins":
        BinningClass = ROOT.DJS.ELogMovEBinning
        objs.pt_desc = 'E [GeV]'
        objs.m_desc = 'log(m/E)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("E", "E", seq(200,1000,200)+seq(1200,4000,400)+[6000], isGeV=True ),
                                   AxisSpec("logmopt", "log(m/E)",[-6,-5.,-4.5, -4.25, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0], isGeV=False), # m<40GeV for all log(m/E)<-4/5
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
                                   )

    if objs.binSystem=="MLogMoEBins":
        BinningClass = ROOT.DJS.MLogMovEBinning
        objs.pt_desc = 'm [GeV]'
        objs.m_desc = 'log(m/E)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("m", "m true",seq(0,200,10)+[225,250,275,300], isGeV=True),
                                   AxisSpec("logmopt", "log(m/E)",[-6,-5.,-4.5,  -4.25, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0], isGeV=False), # m<40GeV for all log(m/E)<-4/5
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
                                   )


    if objs.binSystem=="EtLogMoEtBins":
        BinningClass = ROOT.DJS.EtLogMovEtBinning
        objs.pt_desc = 'E [GeV]'
        objs.m_desc = 'log(m/E)'
        jmsBPS = BinnedPhaseSpace( "JMSbins",
                                   # bins from BinHadler/BinHandler.cxx
                                   AxisSpec("Et", "Et", seq(200,1000,200)+seq(1200,4000,400)+[6000], isGeV=True ),
                                   AxisSpec("logmoet", "log(m/Et)",[-6,-5, -4.5, -4.25, -4, -3.75, -3.5, -3.25, -3,-2.75, -2.5, -2.25, -2,-1.75,-1.5, -1.25,-1,-0.8,0], isGeV=False), # m<40GeV for all log(m/E)<-4/5
                                   AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False)
                                   )



    objs.jmsWorkDir = "jms_"+objs.binSystem+('_noni/' if jmsNIopts.noNumInv else '_ni/')
    objs.jmsQualifier = ""

    # remember what binning class we use.
    objs.BinningClass = BinningClass   

    if jmsNIopts.noNumInv:
        # when NOT doing JMS numerical inversion, we fill the 'numinv' histos directly in the JMS.rawresp step
        # we thus don't need the niresp step
        objs.vetoedCalibSteps = ["JMS.niresp"]

    respBuilder.m_noMassNumInv = jmsNIopts.noNumInv

    respBuilder.m_jmsTrueBin = BinningClass()
    respBuilder.m_jmsRecoBin = BinningClass()

    objs.jmsBPS = jmsBPS
    respBuilder.m_jmsBPS = jmsBPS
    respBuilder.m_jmsRecoBin.m_bps = jmsBPS # for interactive use 
    respBuilder.m_jmsTrueBin.m_bps = jmsBPS # for interactive use 

    # --------

    # --------
    # Define JMS histogram containers .

    objs.mR_in_PtEtaMtrue         = BinnedTH1("mR_in_PtEtaMtrue", jmsBPS, h1(200,0.01,5), Title="Mass Response", var_x='m response')
    objs.mR_in_PtEtaMnuminv       = BinnedTH1("mR_in_PtEtaMnuminv", jmsBPS, h1(200,0.01,5), Title="Mass Response", var_x='m response')
    objs.closure_mR_in_PtEtaMtrue = BinnedTH1("closure_mR_in_PtEtaMtrue", jmsBPS, h1(200,0.01,5), Title="Mass Response closure", var_x='m response')
    objs.m_in_PtEtaMtrue   = ROOT.DJS.VecStats("m_in_PtEtaMtrue", jmsBPS.nBins() )
    objs.m_in_PtEtaMnuminv = ROOT.DJS.VecStats("m_in_PtEtaMnuminv", jmsBPS.nBins() )
    # --------



    # --------
    # define a phase space used in final comparisons 
    jmsinclPtBPS = BinnedPhaseSpace( "JMSinclPt",
                                     AxisSpec("pt", "p_{T} true", seq(200,1000,100)+seq(1200,3000,200), isGeV=True ),
                                     AxisSpec("m", "mass true", [40, 80, 100,140,200], isGeV=True ),
                                     AxisSpec("abseta", "|eta|",jmsEtaBins, isGeV=False),
                                     )
    # define corresponding histogram containers
    objs.mR_in_inclPtEtatrue         = BinnedTH1("mR_in_inclPtEtatrue", jmsinclPtBPS, h1(200,0.01,5), Title="Mass Response", var_x='m response')
    objs.closure_mR_in_inclPtEtatrue = BinnedTH1("closure_mR_in_inclPtEtatrue", jmsinclPtBPS, h1(200,0.01,5), Title="Mass Response", var_x='m response')
    objs.jmsinclPtBPS = jmsinclPtBPS
    # --------

    

    # --------
    # JMS Response fitter.
    balanceFitter_jms = ROOT.JES_BalanceFitter(1.3) #(1.6)
    balanceFitter_jms.SetGaus()
    balanceFitter_jms.SetDefaultMinMax(0. , 4. )
    #balanceFitter_jms.SetFitOpt("RQE0S")
    balanceFitter_jms.SetFitOpt("RWLQ0S") # for JMS, use WL="Weighted Likelihood" because we're not rebinning
    balanceFitter_jms.StopWhenFitFails(True)
    objs.balanceFitter_jms =balanceFitter_jms
    # --------

    # --------
    # JMS smoothers
    from DeriveJetScales.KernelSmoothing import buildSmoother
    jms_th1_smoother = buildSmoother(gausW=0.07, log=[True], npoints=[38] )        
    objs.jms_th1_smoother = jms_th1_smoother

    # for mR vs pt,m smoothing
    if objs.binSystem in ["ELogMoEBins" , "EtLogMoEtBins" ]:
        jms_tg2_smoother = buildSmoother(gausW=0.09, log=[False,False], npoints=[60,60] ,
                                         useErrors=True, maxNpoints=30,
                                         stretchF=(1,1) )        
    else: # objs.binSystem=="PtMBins":
        jms_tg2_smoother = buildSmoother(gausW=0.07, log=[False,False], npoints=[60,60] ,
                                         useErrors=True, maxNpoints=30,
                                         stretchF=(1.3,0.6) )        
        
    objs.jms_tg2_smoother = jms_tg2_smoother
    # --------



    if objs.jmsPreCorr:
        preCorrF = ROOT.TFile("AntiKt4EMTopo_JMS_preCorr.root")            
        mRsmooth_vs_PtMnuminv_in_Eta_preCorr = preCorrF.JMSbins_sub01_mRsmooth_vs_PtMnuminv_in_Eta
        objs.preCorrF = preCorrF
        mRsmooth_vs_PtMnuminv_in_Eta_preCorr.loadHistos(preCorrF)
        respBuilder.setJMSPreCorrFactors(mRsmooth_vs_PtMnuminv_in_Eta_preCorr)    
        objs.mRsmooth_vs_PtMnuminv_in_Eta_preCorr = mRsmooth_vs_PtMnuminv_in_Eta_preCorr


    return objs


# for debuging....
def configureValid(objs):
    def noOwnerShip(o):
        ROOT.SetOwnership(o,False) 
        o.SetDirectory(0)
        return o
    objs.v_pt = noOwnerShip(ROOT.TH1F("pt", "pT", 300, 0, 5000))
    objs.v_m = noOwnerShip(ROOT.TH1F("m", "m", 300, 0, 300))
    objs.v_eta = noOwnerShip(ROOT.TH1F("eta", "eta", 300, -5, 5))
    objs.v_logmopt = noOwnerShip(ROOT.TH1F("logompt", "Log(m/pT)", 300, -6, 0))
    objs.v_mR_vs_logmopt = noOwnerShip(ROOT.TH2F("mR_vs_logompt", "mR vs Log(m/pT)", 300, -6, 0, 200,0,5) )
    objs.v_mR_vs_logmopt_reco = noOwnerShip(ROOT.TH2F("mR_vs_logompt_reco", "mR vs Log(m/pT)", 300, -6, 0, 200,0,5) )

    objs.v_logmopt_vs_e = noOwnerShip(ROOT.TH2F("logompt_pt", "Log(m/pT) vs E", 150, 200, 5000, 150, -6, 0))
    objs.v_m_vs_pt = noOwnerShip(ROOT.TH2F("m_pt", "m vs pT", 150,200, 3000, 150, 0, 300))
