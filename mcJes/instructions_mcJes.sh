## Create a directory for this framework
mkdir DeriveJES
cd DeriveJES
mkdir build source
cd source/

## Clone DeriveJetScales and its dependencies' packages (JES\_ResponseFitter and BinnedPhaseSpace)
git clone ssh://git@gitlab.cern.ch:7999/atlas-jetetmiss-jesjer/MCcalibrations/DeriveJetScales.git # master (1)
git clone ssh://git@gitlab.cern.ch:7999/alinss/DeriveJetScales.git # arthur master (2)
git clone --branch bspline ssh://git@gitlab.cern.ch:7999/alinss/DeriveJetScales.git # arthur special branch bspline (3)

## Clone other
git clone ssh://git@gitlab.cern.ch:7999/atlas-jetetmiss-jesjer/tools/JES_ResponseFitter.git
git clone ssh://git@gitlab.cern.ch:7999/atlas-jetetmiss/BinnedPhaseSpace.git

## set AB
asetup AnalysisBase,21.2.171,here # (1)(2)
asetup AnalysisBase,21.2.103,here # (2)(3)

## Compile (1)(2)
cd ../build
cmake ../source
make -j
source x86*/setup.sh
cd ..
## works fine.

## Compile (3)
## Instructions Here: https://gitlab.cern.ch/alinss/DeriveJetScales/-/tree/bspline/
## and here: https://github.com/bgrimstad/splinter/blob/develop/docs/compile.md
## Inside the master directory `/mcJes`:
git clone https://github.com/bgrimstad/splinter.git
cd splinter/
mkdir build && cd build
export CXX=$(which g++)
cmake ..
make
make install
## I get this permission error: https://github.com/bgrimstad/splinter/blob/develop/docs/compile.md#troubleshooting


## Copy a working config file that has all the magic to run he MCJES code
cp /eos/user/g/gotero/JES_samples/TLA/online_TLA.py ./source/DeriveJetScales/scripts/ # (1)

## Pre-run
## In my `/run` dir I should have:
deriveJES.py      # python
online_TLA.py     # config file
AtlasStyleUtils.C # plotting
## These are worthless (I think):
AntiKt10LCTopoTrimmedPtFrac5SmallR20.py  AntiKt4EMTopo_combined.py AntiKt10LCTopoTrimmedPtFrac5SmallR20_finebins.py  AntiKt4EMTopo_finebins.py AntiKt4EMTopo.py

### Make sure deriveJES.py is set to batchMode True

### Run (remember, the tree should be reweighted!)
# cd source/DeriveJetScales/scripts/ # (1)
# EXAMPLE:
# python deriveJES.py <config> --inputPattern '<PathToTree>' -t <tree_name> -s EtaJES &
# python deriveJES.py <config> --inputPattern '<PathToTree>' -t <tree_name> -s Plots.jes &
#
# The first step generates <file_name>_JES.root.
# The second step generates <file_name>_JES_constants.config, <file_name>_JES_constants.py and <file_name>_JES.pdf.
# The first two have the parameters of the fits bin by bin and the last one is a pdf with all the plots.
#
#python deriveJES.py online_TLA --inputPattern '/eos/user/g/gotero/JES_samples/TLA/MC16d_PythiaWithSW/trees/mc16d.root' -t IsolatedJet_tree -s EtaJES &
#python deriveJES.py online_TLA --inputPattern '/eos/user/g/gotero/JES_samples/TLA/MC16d_PythiaWithSW/trees/mc16d.root' -t IsolatedJet_tree -s Plots.jes &

## Copy the file online_TLA_JES.pdf to the local computer (via scp) and enjoy the output




## Now some example runs:

## 1º python r22:
#python deriveJES.py online_TLA --inputPattern '/eos/user/g/gotero/JES_samples/TLA/MC16d_PythiaWithSW/trees/mc16d.root' -t IsolatedJet_tree -s EtaJES >Log_EtaJes 2>Err_EtaJes &
## 2º python r22:
#python deriveJES.py online_TLA --inputPattern '/eos/user/g/gotero/JES_samples/TLA/MC16d_PythiaWithSW/trees/mc16d.root' -t IsolatedJet_tree -s Plots.jes >Log_Plots 2>Err_Plots &

## 1º python:
#python deriveJES.py online_TLA --inputPattern '/eos/user/g/gotero/JES_samples/TLA/MC16d_PythiaWithSW/trees/mc16d.root' -t IsolatedJet_tree -s EtaJES >Log_EtaJes 2>Err_EtaJes &
#python deriveJES.py online_TLA --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput/tree/user.lbazzano.361021.Pythia_test1__130721_tree.root' -t IsolatedJet_tree -s EtaJES >Log_EtaJes 2>Err_EtaJes &
#python deriveJES.py online_TLA --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput_r22/tree/*tree.root' -t IsolatedJet_tree -s EtaJES >Log_EtaJes 2>Err_EtaJes &
#python deriveJES.py online_TLA --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput_r22/MC16_R22_tree.root' -t IsolatedJet_tree -s EtaJES >Log_EtaJes 2>Err_EtaJes &
#python deriveJES.py online_TLA --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput_r22_files_wCalib2/MC16_r22_tree.root' -t IsolatedJet_tree -s EtaJES >Log_EtaJes 2>Err_EtaJes &

python deriveJES.py AntiKt4EMPFlow --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput_r22_files_wCalib2/MC16_r22_tree.root' -t IsolatedJet_tree -s EtaJES >Log_EtaJes 2>Err_EtaJes &

## 2º python:
#python deriveJES.py online_TLA --inputPattern '/eos/user/g/gotero/JES_samples/TLA/MC16d_PythiaWithSW/trees/mc16d.root' -t IsolatedJet_tree -s Plots.jes >Log_Plots 2>Err_Plots &
#python deriveJES.py online_TLA --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput/tree/user.lbazzano.361021.Pythia_test1__130721_tree.root' -t IsolatedJet_tree -s Plots.jes >Log_Plots 2>Err_Plots &
#python deriveJES.py online_TLA --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput_r22/MC16_R22_tree.root' -t IsolatedJet_tree -s Plots.jes >Log_Plots 2>Err_Plots &
#python deriveJES.py online_TLA --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput_r22_files_wCalib2/MC16_r22_tree.root' -t IsolatedJet_tree -s Plots.jes >Log_Plots 2>Err_Plots &

python deriveJES.py AntiKt4EMPFlow --inputPattern '/eos/user/l/lbazzano/QT/r22_isolatedEff/runGrid/gridOutput_r22_files_wCalib2/MC16_r22_tree.root' -t IsolatedJet_tree -s Plots.jes >Log_Plots 2>Err_Plots &

# JMS:
# python deriveJES.py online_TLA --inputPattern '/eos/user/g/gotero/JES_samples/TLA/MC16d_PythiaWithSW/trees/mc16d.root' -t IsolatedJet_tree >Log_JesJmsPlots 2>Err_JesJmsPlots &
