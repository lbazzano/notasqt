import ROOT
#import sys
#import matplotlib.pyplot as plt
import numpy as np
#import time
#from tqdm import tqdm
import distutils.dir_util
##############################################################################
# open
masterFolder = "../results/"

#inFileFolder = "run_r22_AntiKt4EMPFlow"
#inFileFolder = "run_r22_AntiKt4EMPFlow_noGrooming"
#inFileFolder = "run_r22_test1"
inFileFolder = "run_r22_test2"
inFileFolder = "run_r22_test3"

inFileFolder = masterFolder+inFileFolder

#inFileName = inFileFolder+"/online_TLA_JES.root"
#inFileName = inFileFolder+"/online_TLA_JES.root"
#inFileName = inFileFolder+"/AntiKt4EMPFlow_NoPtCut_JES.root"
inFileName = inFileFolder+"/AntiKt4EMPFlow_JES.root"
inFileName = inFileFolder+"/AntiKt4EMPFlow_JES.root"

# method
fittingType = "polBestChi2+groom"
#fittingType = "polBestChi2"

# variable
variable = "Response"
#variable = "Closure"

##############################################################################

Response = False
Closure = False
if variable == "Response":
  var         = "R_vs_Enuminv"
  Response = True
  Title = "Fitted Response"
if variable == "Closure":
  var         = "closure_R_vs_Etrue"
  Closure = True
  Title = "Closure"

# save
#folder = "plotted_responses"
#folder = "plotted_responses_nogroom"
folder = inFileFolder+"/"+fittingType+"_"+var
distutils.dir_util.mkpath(folder)


# graphs
Graph       = var+"_Graph"          #response / Calibrated Response
# functions
Func        = var+"_Func"           #fitted response
if "groom" in fittingType:
  SLGraph     = var+"_SLGraph"        #response fit points
  GroomFunc   = var+"_GroomExt_Func"  #groom fit

# eta bins
Bins        = np.arange(0, 90, 1)
Etas        = np.arange(-4.50,4.51,0.1)

ROOT.gROOT.SetBatch(True)
inFile = ROOT.TFile.Open(inFileName ," READ ")
inFile.cd()

a=10
b=100000

for Bin in Bins:
      c1 = ROOT.TCanvas( "c"+str(Bin), "c"+str(Bin), 1000, 400 )
      c1.cd()
      ROOT.gPad.SetLogx();
      mg = ROOT.TMultiGraph("mg",Title)
      latex = ROOT.TLatex()
      try:
        gr     = inFile.Get(Graph+"_"+str(Bin))
        if Response:
          legend = ROOT.TLegend(0.6 ,0.25 ,0.85 ,0.45)
          legend.AddEntry( gr  ,"Response")
          gr.SetLineWidth(0)
          gr.SetMarkerStyle(8)
          gr.SetMarkerColor(1)
          gr.SetMarkerSize(0.5)
        if Closure:
          legend = ROOT.TLegend(0.55 ,0.65 ,0.75 ,0.85)
          legend.AddEntry( gr  ,"Calibrated Response")
          gr.SetMarkerStyle(4)
          gr.SetMarkerColor(2)
          gr.SetMarkerSize(0.5)
          gr.SetLineColor(2)
        mg.Add(gr)
      except:
        NameError
      try:
        slgr   = inFile.Get(SLGraph+"_"+str(Bin))
        slgr.SetMarkerStyle(4)
        slgr.SetMarkerColor(2)
        slgr.SetLineWidth(0)
        mg.Add(slgr)
        legend.AddEntry( slgr,"Response Fit Points")
      except:
        NameError

      mg.GetYaxis().SetTitle("Fitted Response")
      if Response:
        mg.GetXaxis().SetLimits(a,b)
        mg.GetYaxis().SetRangeUser(0.1,1.1)
        mg.GetXaxis().SetTitle("E_{reco (NI)} [GeV]")
        latex.DrawLatex(2000 ,0.2 , "Fitted response vs E_{reco (NI)}")
        latex.Draw("same")
        L1y = 0.2
        L2y = 0.15
        mg.Draw("APL")
      if Closure:
        mg.GetXaxis().SetLimits(a,b)
        mg.GetYaxis().SetRangeUser(0.96,1.1)
        #mg.GetYaxis().SetRangeUser(0.1,1)
        mg.GetXaxis().SetTitle("E_{true} [GeV]")
        latex.DrawLatex(2000 ,0.95 , "Closure vs E_{true}")
        latex.Draw("same")
        L1y = 1.09
        L2y = 1.08
        mg.Draw("APZ")
      

      try:
        f      = inFile.Get(Func+"_"+str(Bin))
        f.SetLineColor(4)
        f.Draw("same")
        legend.AddEntry( f   ,"Fitted Response")
      except:
        NameError 
      try:
        fg     = inFile.Get(GroomFunc+"_"+str(Bin))
        fg.SetLineStyle(7)
        fg.Draw("same")
        legend.AddEntry( fg  ,"Groom Fit")
      except:
        NameError
      line1 = ROOT.TLine(a,1,b,1)
      line1.SetLineColor(1)
      line1.Draw("same")
      line2 = ROOT.TLine(a,0.99,b,0.99)
      line2.SetLineColor(1)
      line2.SetLineStyle(10)
      line2.Draw("same")
      line3 = ROOT.TLine(a,1.01,b,1.01)
      line3.SetLineColor(1)
      line3.SetLineStyle(10)
      line3.Draw("same")
      
      legend.SetLineWidth(0)
      legend.Draw(" same ")

      latex.SetTextSize(0.05)
      latex.DrawText(2000 ,L1y , "AntiKt4EMPFlow")
      latex.Draw("same")
      latex.SetTextSize(0.04)
      latex.DrawLatex(2000,L2y, str(round(Etas[Bin],2))+"< \eta <"+str(round(Etas[Bin+1],2)))
      latex.Draw("same")
      
      c1.SetGrid()
      c1.SaveAs(folder+"/Bin_"+str(Bin)+".png")

inFile.Close()
