## Create a directory 
mkdir r22_isolatedJetTree
cd r22_isolatedJetTree
mkdir source
mkdir build

## Clone IsolatedJetTree and chechout r22
cd source
git clone ssh://git@gitlab.cern.ch:7999/atlas-jetetmiss-jesjer/MCcalibrations/IsolatedJetTree.git
cd IsolatedJetTree/
git checkout R22-master
cd ..

## Clone xAH and checkout r22
git clone https://github.com/UCATLAS/xAODAnaHelpers.git
cd xAODAnaHelpers/
git checkout r22/master
cd ..

## CMakeLists must be manually added for some technical issues
cp /eos/atlas/atlascerngroupdisk/trig-jet/CMakeLists_R22/CMakeLists.txt .
cd ..

## Compile
setupATLAS
cd source/
asetup AnalysisBase,22.2.23
cd ../build
cmake ../source
make -j3
source */setup.sh
cd ..


## In order to incorporate truth information to the output tree, one needs to add these lines to the config file:
addTruthJets = True
if addTruthJets:
    # Reconstruct small-R truth jets
    import JetRecConfig.JetAnalysisCommon
    from JetRecConfig.JetRecConfig import JetRecCfg
    from JetRecConfig.StandardSmallRJets import AntiKt4EMPFlow, AntiKt4LCTopo, AntiKt4Truth
    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Input.isMC=True
    ConfigFlags.Input.Collections = ["TruthEvents"]
    acc=JetRecCfg(AntiKt4Truth, ConfigFlags)
    c._algorithms += [ a.asAnaAlg() for a in acc.algs ] 

## One needs to add some packages to the source:
setupATLAS
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git
cd athena
git atlas addpkg JetRecConfig
git atlas addpkg JetRecTools
git atlas addpkg JetRec
git atlas addpkg AnaAlgorithm
git atlas addpkg EventShapeTools
git atlas addpkg JetCalibTools
git atlas addpkg JetMomentTools
git atlas addpkg ParticleJetTools
git checkout release/22.2.24
## then put these packages inside the source directory

## To avoid errors, 'mass' should be included in the detailStr variable:
    "m_jetDetailStr"    : "kinematic JetConstitScale JetPileupScale JetJESScale JetGSCScale mass",

## Run
mkdir run
cd run
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config config_file.py --files sample_r22.root --submitDir testing_sample_r22 --force direct >Log 2>Err &

## Run on the grid:
python source/IsolatedJetTree/scripts/runGrid.py

## Download from grid:
python ../source/IsolatedJetTree/scripts/downloadAndMerge.py --types tree --container user.lbazzano:user.lbazzano.361021.Pythia_test1__130721_tree.root --outPath conCalib_gridOutput
python ../source/IsolatedJetTree/scripts/downloadAndMerge.py --types tree --file r22_files_wCalib2 --outPath gridOutput_r22_files_wCalib2

## Always reweight:
python computeWeight.py


## The end ##

 --files:
/eos/atlas/atlascerngroupdisk/trig-jet/R22ExampleSample_wTruthJets/myxAOD.pool.root
 --config:
/eos/atlas/atlascerngroupdisk/trig-jet/R22_analysisConfigs/IsolatedJetTree/Configs/config_R22.py



## Example runs ##
#
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/test_sampleR22/myxAOD.pool.root --submitDir test_truth_sampleR22 --force direct >Log 2>Err &
#
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/valid1.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.recon.AOD.e3569_s3126_d1650_r12665/AOD.25702895._000123.pool.root.1 --submitDir mini_sampleR22_testForGrid_alternative --force direct >Log 2>Err &
#
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir mcjes_test1 --force direct >Log 2>Err &

## Tests mariana
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withoutCalib2 --force direct 
#
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withCalib2 --force direct 

## Tests post mariana
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withoutCalib3 --force direct 
#
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withCalib3 --force direct 

## Tests post flor
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withoutCalib4 --force direct 
#
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withCalib4 --force direct 

## Local config tests
## cloud
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withCalib_nube --force direct 
#
## local
# python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withCalib_local --force direct 
#
## local with my own mcjes
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withCalib_local_Apr_mcJes_JetArea_Residual --force direct 
#
#python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withCalib_cloud_Apr_mcJes_JetArea_Residual --force direct 
#
python ../source/xAODAnaHelpers/scripts/xAH_run.py --config /eos/user/l/lbazzano/QT/r22_isolatedEff/configs_r22/IsolatedJetTree/Configs/config_R22.py --files /eos/user/l/lbazzano/QT/samples/mc16_13TeV/AOD.26391598._020971.pool.root.1 --submitDir test_withoutCalib_cloud_Apr_mcJes_JetArea_Residual --force direct 
