import ROOT
import sys
import matplotlib.pyplot as plt
#from tqdm import tqdm

##############################################################################
inFileNames = [
"user.arombola.800036.MC16_r22Agustin_withJetCalib_080921_tree.root",
"user.arombola.364702.MC16_r22Agustin_withJetCalib_080921_tree.root",
"user.arombola.364703.MC16_r22Agustin_withJetCalib_080921_tree.root",
"user.arombola.364704.MC16_r22Agustin_withJetCalib_080921_tree.root",
"user.arombola.364705.MC16_r22Agustin_withJetCalib_080921_tree.root",
"user.arombola.364706.MC16_r22Agustin_withJetCalib_080921_tree.root",]

Slices      = ["1","2","3","4","5","6",]

## variable
'''
var         = "jet_pt"
var         = "jet_PileupPt"
var         = "jet_GSCPt"
var         = "jet_eta"
'''
print('variable: ')
var = raw_input()
print(var)
##############################################################################

outFileName = var+"_outputFile.root"

if "pt" or "Pt" in var:# == "jet_pt":
  n_bins  = 1000
  lim_inf = 0    # GeV
  lim_max = 3000 # GeV
if var == "jet_eta":
  lim_inf = -5 #rapidity
  lim_max = 5  #rapidity

s=0
for inFileName,Slice in zip(inFileNames,Slices):
  hist_var     = ROOT.TH1D("data",var+",data",n_bins,lim_inf,lim_max)
  if s==0:
    hist_var_tot = ROOT.TH1D("total data",var+",total data",n_bins,lim_inf,lim_max)
  print("... Reading tree from "+inFileName)
  inFile = ROOT.TFile.Open(inFileName ," READ ")
  inFile.cd()
  tree = inFile.Get("IsolatedJet_tree")

  for entry in range(0,int(tree.GetEntries()/1000)):
    tree.GetEntry(entry)
    variable  = getattr(tree,var)[0]
    weight    = getattr(tree,"weightreweighted")
    hist_var.Fill(variable,weight) # fill histo with weights
    hist_var_tot.Fill(variable,weight) # fill histo with weights
  hist_var.SetDirectory(0) # set parent file to none
  inFile.Close()
  if s==0:
      outHistFile = ROOT.TFile.Open(outFileName,"RECREATE")
      s+=1
  outHistFile.cd()
  hist_var.Write(var+"_Slice_"+Slice)
hist_var_tot.Write(var+"_AllSlices")
outHistFile.Close()

