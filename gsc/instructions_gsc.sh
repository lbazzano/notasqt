# Variables
# Longitudinal:
fLAr3  = E(LAr3)/E(jet)      (energía EM en general)
fTile0 = E(Tile0)/E(jet)
# Transverse
Wtrk = sum()


## Install ##
# Clon Gustavo's previous workspace
git clone ssh://git@gitlab.cern.ch:7999/gotero/gsctree.git
cd gsctree
cd source
# Clone needed repositories
git clone ssh://git@gitlab.cern.ch:7999/atlas-jetetmiss-jesjer/MCcalibrations/GSC.git
git clone https://:@gitlab.cern.ch:8443/atlas-jetetmiss-jesjer/tools/JES_ResponseFitter.git
git clone https://github.com/UCATLAS/xAODAnaHelpers.git

## Setup ##
# rm build since Setup.sh will create one:
rm -r ../build
vim Setup.sh
# Make sure:
# asetup AnalysisBase,21.2.171,here
# source x86_64-centos7-gcc8-opt/setup.sh
source Setup.sh
# aprox 5 min


## Run ##
# run from txt with all listed files:
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files testFile.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0 --log-level debug --force direct
# or run from a sinlge rootfile:
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files /eos/user/l/lbazzano/QT/isolatedEff/run/submitDir/data-tree/mc16_13TeV.root --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0 --log-level debug --force direct
# this takes about 10 min and yields in /GSCTest1:
#       hist-files.root


## Fitter ##
# Outputs depend on the applied correction:
python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0/hist-files.root #fit tile0
# Tile0 outputs:
#      Fitted_Tile0_hist-files.root
#      PlotDump_Fitter_response_Tile0.pdf y PlotDump_Fitter_Var_Tile0.pdf
python GSC/scripts/GSCfitter.py -b --correction Inclusive --input /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0/hist-files.root #fit no calib
# Inclusive outputs:
#      Fitted_Inclusive_hist-files.root
#      PlotDump_Fitter_response_Inclusive.pdf

## Smoother ##
python GSC/scripts/GSCsmoother.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/GSCTest1_Tile0/Fitted_Tile0_hist-files.root


## Plots ##
# (remember to set Batch mode on!!!)
# plotGSCImprovement.py plots the improvement of the central response and resolution across different GSC stages (as opposed to the previous 2 plotting scripts, which each dealt with a single stage at a given iteration). The input files (inputFiles), the name of their stages (corrTypes), and their colors (fileColors) are each hardcoded at the beginning of the code, and should be set by the user. This code also runs over differnt flavor types, including light-quarks, gluons, b-quarks, c-quarks, and inclusively. The output plots are put in the plots/ directory in the same directory that the code is run in.
python source/GSC/scripts/plotGSCImprovement.py --path . --outDir improv --jetType EM
# plotGSCResponse.py plots the usual pt response curves verses VAR for several pt regions in a given eta bin.
python source/GSC/scripts/plotGSCResponse.py --input r21GSC_Tile0/Fitted_Tile0_hist-files.root --outDir Tile0_Tile0 --correction Tile0
# plotSmoothedCurves.py plots the smoothed response curves as a function of pt or of eta, and is ismilar to plotGSCResponse.py. The input is the smoothed histograms that are the output of GSCsmoother.py. The --pts and --etas options work similarly to plotGSCResponse.py, and are "all" by default.
python source/GSC/scripts/plotSmoothedCurves.py --input r21GSC_Tile0/Smoothed_Fitted_Tile0_hist-files.root --outDir smoothed --correction Tile0




## Example Run ##

source Setup.sh

# NONE ################################################################################################################################################################
# in the config:
#  "m_GSCFile"             : "",
#  "m_GSCOrder"            : "",
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/r21GSC --log-level debug --force direct
# Tile0
python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC/hist-files.root
python GSC/scripts/GSCsmoother.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC/Fitted_Tile0_hist-files.root
# Then create the Tile0-calibration rootfile that will be used for correcting Tile0. (***)
cp ../r21GSC/Smoothed_Fitted_Tile0_hist-files.root GSC/data/GSCcalib_r21GSC.root


# TILE0 ###############################################################################################################################################################
# in the config:
#  "m_GSCFile"             : "GSC/data/GSCcalib_r21GSC.root",
#  "m_GSCOrder"            : "Tile0",
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0 --log-level debug --force direct
# Tile0
python GSC/scripts/GSCfitter.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0/hist-files.root
python GSC/scripts/GSCsmoother.py -b --correction Tile0 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0/Fitted_Tile0_hist-files.root
# EM3
python GSC/scripts/GSCfitter.py -b --correction EM3 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0/hist-files.root
python GSC/scripts/GSCsmoother.py -b --correction EM3 --input /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0/Fitted_Tile0_hist-files.root
# hadd Smoothed file with the calibration rootfile obtained before (delete the one from the previous step(***)).
hadd source/GSC/data/GSCcalib_r21GSC.root r21GSC/Smoothed_Fitted_Tile0_hist-files.root r21GSC_Tile0/Smoothed_Fitted_EM3_hist-files.root


# TILE0 + EM3 #########################################################################################################################################################
# in the config:
#  "m_GSCFile"             : "GSC/data/GSCcalib_r21GSC.root",
#  "m_GSCOrder"            : "Tile0,EM3",
python ./xAODAnaHelpers/scripts/xAH_run.py -f --treeName IsolatedJet_tree --files files.txt --inputList --config GSC/data/config_Hist_GSC.py --submitDir /eos/user/l/lbazzano/QT/gsctree/r21GSC_Tile0_EM3 --log-level debug --force direct


#  and repeat until the corrections on all variables are done...

