# Main

**Member:** Lisandro Tomas Bazzano Hurrell  
**Institute:** Buenos Aires  
**Institute representative(s):** Ricardo Piegaia  
**Qualification project:** Trigger  
**Project responsible(s):** Arantxa Ruiz Martinez, Tim Martin  
**Project task:**  
**Task convener(s):**  
**Project description:** This project is to derive dedicated JES/GSC calibrations for HLT jet trigger collections in Run 3. The student will first create ntuples starting from Monte Carlo AODs, and he will then use these ntuples to study energy corrections to be applied to jets in order to match the truth jet energy scale. Once a parametrization of these corrections will be derived, a closure test of the new calibration will be performed through plots of appropriate metrics. A comparisons with the latest offline jet calibrations will also be made. Lastly, new online parametrizations will be propagated to dedicated configuration files of the jet calibration tool, allowing them to be used inside the athenaMT trigger framework. Progress will be documented in a dedicated JIRA ticket, as well as in jet trigger meeting talks. A final talk summarising the QT results will also be presented at the Trigger General Meeting. Lastly, the student will provide documentation of code packages developed to perform the task.  
**Local supervisor:** Gustavo Otero Y Garzon  
**Technical supervisor:** Ayana Tamu Arce  
**Proposed beginning of qualification:** 10/09/2021

1. Determine MCJES and GSC calibrations for the AntiKt4EMPFlow online collection (Jets calibrated up to the Pileup Scale). Get a JES online Calibration.
2. Repeat for the offline collection. Get a JES offline Calibration.
3. Calibrate online collection using the offline. Compare online and offline calibrations (Closure/Resolution plots, jes uncertainty). 
4. Answer: Is it necessary to generate an online calibration, or is the offline calibration enough?

First results: [Jet Signature Group Meeting 11/10/21](https://indico.cern.ch/event/1085861/#sc-36-3-calibrations)

## Code (r22/Run3)
* JetTriggerEfficiency???

* IsolatedJetTree
  * [IsolatedJetTree](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/MCcalibrations/IsolatedJetTree)
  * [xAODAnaHelpers](https://github.com/UCATLAS/xAODAnaHelpers)
  * From [Athena](https://gitlab.cern.ch:8443/atlas/athena):
    * JetRecConfig
    * JetRecTools
    * JetRec
    * AnaAlgorithm
    * EventShapeTools
    * JetCalibTools
    * JetMomentTools
    * ParticleJetTools

* MCJES
  * [DeriveJetScales](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/MCcalibrations/DeriveJetScales)
  * [JES\_ResponseFitter](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/tools/JES_ResponseFitter)
  * [BinnedPhaseSpace](https://gitlab.cern.ch/atlas-jetetmiss/BinnedPhaseSpace)

* GSC:
  * [gsctree](https://gitlab.cern.ch/gotero/gsctree)
  * [GSC](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/MCcalibrations/GSC)
  * [JES\_ResponseFitter](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/tools/JES_ResponseFitter)
  * [xAODAnaHelpers](https://github.com/UCATLAS/xAODAnaHelpers)


## Summary

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ2FudHRcbiAgICB0aXRsZSBXb3JrZmxvd1xuICAgIGRhdGVGb3JtYXQgIFlZWVktTU0tRERcbiAgICBheGlzRm9ybWF0ICVcbiAgICBcbiAgICBzZWN0aW9uIElzb2xhdGVkIEpldCBUcmVlXG4gICAgSExUIG5vQ2FsaWIgSmV0cyArIEpldEFyZWEgKyBSZXNpZHVhbCBvZmZsaW5lICA9IFBpbGV1cFNjYWxlIEpldHMgICAgICAgICAgOmExLCAwMDAwLTAxLTAxLCAzZFxuICAgIEhMVCBub0NhbGliIEpldHMgKyBKZXRBcmVhICsgUmVzaWR1YWwgb2ZmbGluZSArIE1DSkVTIE9ubGluZSA9ICBKRVNTY2FsZSBKZXRzICAgICAgICAgIDphMyxhZnRlciBhMiwgNGRcbiAgICBITFQgbm9DYWxpYiBKZXRzICsgSmV0QXJlYSArIFJlc2lkdWFsIG9mZmxpbmUgKyBNQ0pFUyBPbmxpbmUgKyBHU0MgT25saW5lID0gR1NDU2NhbGUgSmV0cyAgICAgICAgICA6YWZ0ZXIgYTQsIDRkXG4gICAgc2VjdGlvbiBNQ0pFU1xuICAgIFBpbGV1cFNjYWxlIEpldHMgLT4gSkVTIENhbGlicmF0aW9uICAgICAgOmEyLGFmdGVyIGExICAsIDJkXG4gICAgc2VjdGlvbiBHU0NcbiAgICBKRVNTY2FsZSBKZXRzIC0-IEdTQyBDYWxpYnJhdGlvbiAgICAgIDphNCxhZnRlciBhMyAsIDJkICAgICAgICAgICBcbiAiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlLCJhdXRvU3luYyI6dHJ1ZSwidXBkYXRlRGlhZ3JhbSI6ZmFsc2V9)](https://mermaid-js.github.io/mermaid-live-editor/edit/#eyJjb2RlIjoiZ2FudHRcbiAgICB0aXRsZSBXb3JrZmxvd1xuICAgIGRhdGVGb3JtYXQgIFlZWVktTU0tRERcbiAgICBheGlzRm9ybWF0ICVcbiAgICBcbiAgICBzZWN0aW9uIElzb2xhdGVkIEpldCBUcmVlXG4gICAgSExUIG5vQ2FsaWIgSmV0cyArIEpldEFyZWEgKyBSZXNpZHVhbCBvZmZsaW5lICA9IFBpbGV1cFNjYWxlIEpldHMgICAgICAgICAgOmExLCAwMDAwLTAxLTAxLCAzZFxuICAgIEhMVCBub0NhbGliIEpldHMgKyBKZXRBcmVhICsgUmVzaWR1YWwgb2ZmbGluZSArIE1DSkVTIE9ubGluZSA9ICBKRVNTY2FsZSBKZXRzICAgICAgICAgIDphMyxhZnRlciBhMiwgNGRcbiAgICBITFQgbm9DYWxpYiBKZXRzICsgSmV0QXJlYSArIFJlc2lkdWFsIG9mZmxpbmUgKyBNQ0pFUyBPbmxpbmUgKyBHU0MgT25saW5lID0gR1NDU2NhbGUgSmV0cyAgICAgICAgICA6YWZ0ZXIgYTQsIDRkXG4gICAgc2VjdGlvbiBNQ0pFU1xuICAgIFBpbGV1cFNjYWxlIEpldHMgLT4gSkVTIENhbGlicmF0aW9uICAgICAgOmEyLGFmdGVyIGExICAsIDJkXG4gICAgc2VjdGlvbiBHU0NcbiAgICBKRVNTY2FsZSBKZXRzIC0-IEdTQyBDYWxpYnJhdGlvbiAgICAgIDphNCxhZnRlciBhMyAsIDJkICAgICAgICAgICBcbiAiLCJtZXJtYWlkIjoie1xuICBcInRoZW1lXCI6IFwiZGVmYXVsdFwiXG59IiwidXBkYXRlRWRpdG9yIjpmYWxzZSwiYXV0b1N5bmMiOnRydWUsInVwZGF0ZURpYWdyYW0iOmZhbHNlfQ)

We may have to determine Area+Residual calibration too (ignoring InSitu, Smearing)

# Notes
## Isolation details
When studying Response, a requirement is needed for isolated jets:
* `Reco ` Jets shouldn't have a significative (pt > 7 GeV) nearby (1.5\*R).
* `Truth` Jets shouldn't have a significative (pt > 7 GeV) nearby (2.5\*R).

## Jet container
AntiKt4EMPFlow

## Calibration Details
### JetCalibrator
The sample includes information on all jet calibration steps, called scales. The calibConfig file used in the sample Derivation that included our sample is [here](https://gitlab.cern.ch/atlas/athena/-/blob/master/Reconstruction/Jet/JetCalibTools/python/JetCalibToolsConfig.py#L26), and for our sample [this config file](https://atlas-groupdata.web.cern.ch/atlas-groupdata/JetCalibTools/CalibArea-00-04-82/CalibrationConfigs/JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21_Trigger.config) was used. The way of seeing these scales in the tree is including this in the IsolatedJetTree Algorithm:
```python
    "m_jetDetailStr"    : "kinematic JetConstitScale JetPileupScale JetJESScale JetGSCScale mass",
```
For example, the tree will obtain JetPileupScale which was initially defined in the Derivation.


* In the case one wants PileupScale jets (to determine MCJES for example), these will be calibrated based on the offline calibration defined inside the sample. This is the way to do so:
```python
    calibSeq = "JetArea_Residual"
    CalibConfig = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21_Trigger.config"
```
If the JetCalibrator Algorithm is not included in the config file, the tree `jet\_pt` variable will correspond to the final scale. If the calibrator is inlcuded with a `calibSeq = JetArea_Residual`, the variable`jet_pt` will be the same as `jet_PileupPt`.

* In the case one wants JESScale jets (to determine GSC for example), these can be calibrated based on the offline calibration defined inside the sample. This is the way to do so:
```python
    calibSeq = "JetArea_Residual_EtaJES"
    CalibConfig = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21_Trigger.config"
```
One can also use a local calibration file:
```python
    calibSeq = "JetArea_Residual_EtaJES"
    CalibConfig = "JES_MC16Recommendation_Consolidated_PFlow_Sep2021_Rel21_Trigger.config"
```
by setting:
```python
    "m_jetCalibToolsDEV"       : True,   #Developer-level JetCalibTools config file 
```
in the JetCalibrator Algorithm. This tells the Algorithm to look for all calibration files inside `JetCalibTools/share` (masters and minis). When creating the share folder and  adding calibration files to it, one extra step should be added to the `CMakeFiles` from JetCalibTools:
```json
atlas_install_generic( share/* DESTINATION data/JetCalibTools )
```
This way, the JetCalibTools folder containing the share files appears in the build.



### Jet Selector:
When selecting jets, the correct variable is not:
```python
    #"m_jetScaleType"            :  "JetConstitScaleMomentum",
```
This would _select_ jets based not on the `JetConstitScaleMomentum` but on the default `Final` scale, which probably is `JetGSCScaleMomentum`. So we corrected by adding the _real_ variable:
```python
    "m_jetScale4Selection"      :  "JetConstitScaleMomentum",
```
This correctly _selects_ jets based on the `JetConstitScaleMomentum` (Residual).
This mistake was discovered by looking into the cutflow rootfile (filled when JetSelector.cxx is working). The `jetScaleType` is only used in the DetEta, which is not of our concern, we comment that line.

Esta correción la hicimos mirando el cutflow que se llena dentro de JetSelector.cxx. La variable jetScaleType sólo se utiliza en algo del DetEta que no nos importa mucho, la dejamos comentada.
El error nuestro era, en parte (\*), que estábamos disparando en (creo!):
1. `Residual cuando si poníamos el Calibrator # acá el in container era jets_Calib que estan hasta la residual`
2. `Final    cuando no poníamos el Calibrator # acá el in container era HLT_AntiKt4EMPFlowJets_subjesgscIS_ftf`

### IsolatedJetTree
If we wanted response branches in the tree (calculated from PileupScale Jets):
```python
    #"m_jetScale":"JetPileupScaleMomentum"
```
This is something we don't want right now, so we commented it.




### MCJES:
After this calibration is derived, we should calibrate the sample Jets up to JESScale and use these as GSC inputs.

#### jetCollection
`.config` files coming from the mcJes include the two correction variables: `JES.NAME_BinX` y `EtaCorr.NAME_BinX` where `NAME` should be the `jetCollection` and is usually fixed to  the name of the `jetType` variable or the name of the mcjes-config-file used for the calibration, while the `X` represents the Eta bin (0 is [-0.45,-0.4]). If the name is not right (like `online_TLA`) the command in _vim_ to replace all of these for the jet collection can be used (`:%s/online_TLA/AntiKt4EMPFlow`).

#### EmaxJES
This constant ([some](https://atlas-jet-definitions.web.cern.ch/atlas-jet-definitions/JETDEF.html) [probably](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/DeriveMCJES2015) [useful](https://gitlab.cern.ch/atlas/athena/-/blob/master/Reconstruction/Jet/JetCalibTools/Root/EtaJESCorrection.cxx#L217) [info](https://gitlab.cern.ch/atlas/athena/-/blob/master/Reconstruction/Jet/JetCalibTools/Root/EtaJESCorrection.cxx#L139) freezes the response for each Eta bin so unphysical behaviours given by the response fittings are avoided. It can be activated/deactivated in the IsolatedJetTree master configuration file:
```
AntiKt4EMPFlow.FreezeJEScorrectionatHighE: true/false
```
If set to `false`, response/calibration for high energy jets will be defined by the response-fits, while if `true`, freezing will be done. This variable is not an MCJES output, and it can be calculated using [one of Arthur's scripts](https://gitlab.cern.ch/alinss/DeriveJetScales/-/blob/bspline/scripts/get_E_freeze.py). A minor change should be made to take the **last** response point.
```
npoints = g.GetN()-1   ->   npoints = g.GetN()
```

#### Plotting
To run the Plots stage, Arthur's branch (master) works better than the master original, which fails when _PolBestChi_ alone fits are tested.

### GSC:
After this calibration is derived, we should calibrate the sample Jets up to GSCScale just to see how it works.



## General details/issues/comments:

* We still see differences between the `jet_PileupPt` from the sample (original Derivation) and the one calibrated using the original config file, these should match. Also, there are little differences in the selection of jets.

* When seting up an Analysis Base release, an athena branch is chosen (for example setup AB r21 sets branch 21.2).

* [JetDef Meetings](https://atlas-jet-definitions.web.cern.ch/atlas-jet-definitions/JETDEF.html) with useful info about MCJES PFlow determination.

## Homework

* Understand JES groom.

* Find out what red and blue curves mean in the GCS plots.

* Look for GSC variables inside the AOD/ntuples, think about new variables


<!--- notas/consultas en español:

isolated: do JVT ???

isolated: hay que activar las variables de cleaning ???


### Stage 1 (Tree generation): (add - to arrows)
```mermaid
graph LR
A[HLT Jets no Calib] -> B{IsolatedJetTree}
B -> |JetArea+Residual Offline| C[JetPileupScale Jets]
```
### Stage 2 (MCJES determination):
```mermaid
graph LR
C[JetPileupScale Jets] -> D{MCJES}
A[HLT Jets no Calib] -> B{IsolatedJetTree}
B -> |JetArea+Residual offline + MCJES Online| E[JetJESScale Jets]
```
### Stage 3 (GSC determination):
```mermaid
graph LR
C[JetJESScale Jets] -> D{GSC}
A[HLT Jets no Calib] -> B{IsolatedJetTree}
B -> |JetArea+Residual offline + MCJES Online + GSC Online| E[JetGSCScale Jets]
```

-->
